const express = require("express");
const cors = require("cors");
const app = express();
const movieRouter = require("./routes/movie");
app.use(cors("*"));

app.use(express.json());

app.use("/movie", movieRouter);

app.listen(4545, "0.0.0.0", () => {
  console.log("Server started on port 4545");
});
